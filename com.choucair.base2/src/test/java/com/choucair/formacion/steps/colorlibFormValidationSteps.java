package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;

import net.thucydides.core.annotations.Step;

public class colorlibFormValidationSteps {
	ColorlibFormValidationPage colorlibFormValidationPague;

	
	
		@Step
		public void diligenciar_popup_datos_tabla (List<List<String>> data, int id) {
		colorlibFormValidationPague.Required(data.get(id).get(0).trim());
		colorlibFormValidationPague.Select_Sport(data.get(id).get(1).trim());
		colorlibFormValidationPague.Multiple_Select(data.get(id).get(2).trim());
		colorlibFormValidationPague.Multiple_Select(data.get(id).get(3).trim());
		colorlibFormValidationPague.url(data.get(id).get(4).trim());
		colorlibFormValidationPague.email(data.get(id).get(5).trim());
		colorlibFormValidationPague.password(data.get(id).get(6).trim());
		colorlibFormValidationPague.confirm_password(data.get(id).get(7).trim());
		colorlibFormValidationPague.minimun_field_size(data.get(id).get(8).trim());
		colorlibFormValidationPague.maximun_field_size(data.get(id).get(9).trim());
		colorlibFormValidationPague.number(data.get(id).get(10).trim());
		colorlibFormValidationPague.ip(data.get(id).get(11).trim());
		colorlibFormValidationPague.Date(data.get(id).get(12).trim());
		colorlibFormValidationPague.DateEarlier(data.get(id).get(13).trim());
		colorlibFormValidationPague.validate();
	}
	
		@Step
		public void verificar_ingreso_datos_formulario_exitoso () {
			colorlibFormValidationPague.form_sin_errores();
		}
		
		@Step
		public void verificar_ingreso_datos_formulario_con_errores() {
			colorlibFormValidationPague.form_con_errores();
		}

}
